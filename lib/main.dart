import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:my_fisrt_flutter_app/redux/app_state.dart';
import 'package:my_fisrt_flutter_app/widgets/NewToInjectingBiologics.dart';
import 'package:my_fisrt_flutter_app/widgets/StartScheduleWidget.dart';
import 'package:my_fisrt_flutter_app/widgets/YourAnymedWidget.dart';
import 'package:redux/redux.dart';

import 'redux/actions.dart';
import 'redux/reducers.dart';

void main() {
  final Store<AppState> store = Store(reducer,
      //middleware: [loaderMiddleware],
      initialState:
          AppState(counter: 0, text: "init", widget: Icon(Icons.image)));

  runApp(StoreProvider(
    store: store,
    child: MaterialApp(
        home: Navigator(
      pages: [
        MaterialPage(child: _Counter()),
        if (store.state.widget != null) MaterialPage(child: YourAnymedWidget())
        // if (store.state.widget != null) MaterialPage(child: ImageDetails())
      ],
      onPopPage: (route, result) {
        store.dispatch(ResetWidgetAction());
        return route.didPop(result);
      },
    )),
  ));
}

class _Counter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Store<AppState> store = StoreProvider.of<AppState>(context);
    String inputText = "";

    return Scaffold(
        appBar: AppBar(title: Text("Flutter Redux Navigator 2.0")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () => store.dispatch(GetImageAction(
                        imageURL: "https://picsum.photos/200/300?grayscale")
                    .getImage(store)),
                child: Text("Get image"),
              ),
              Container(
                width: 200,
                child: TextField(
                  onChanged: (text) => inputText = text,
                ),
              ),
              SizedBox(height: 10),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                ElevatedButton(
                    onPressed: () =>
                        store.dispatch(SetTextAction(text: inputText)),
                    child: Text("SET")),
                SizedBox(width: 20),
                ElevatedButton(
                    onPressed: () => store.dispatch(ResetTextAction()),
                    child: Text("RESET")),
              ]),
              StoreConnector<AppState, AppState>(
                converter: (store) => store.state,
                builder: (context, state) => Text(
                  state.text,
                  style: TextStyle(fontSize: 10),
                ),
              ),
              SizedBox(height: 10),
              StoreConnector<AppState, AppState>(
                converter: (store) => store.state,
                builder: (context, state) => Text(
                  state.counter.toString(),
                  style: TextStyle(fontSize: 35),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FloatingActionButton(
                    onPressed: () => store.dispatch(AddAction()),
                    child: Icon(Icons.add),
                  ),
                  SizedBox(width: 20),
                  FloatingActionButton(
                    onPressed: () => store.dispatch(RemoveAction()),
                    child: Icon(Icons.remove),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}

class ImageDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image details'),
      ),
      body: Center(
        child: Container(
            width: 150,
            height: 150,
            margin: const EdgeInsets.all(16),
            child: StoreConnector<AppState, AppState>(
                converter: (store) => store.state,
                builder: (context, state) =>
                    state.widget ?? Icon(Icons.image))),
      ),
    );
  }
}
