import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:my_fisrt_flutter_app/design/AppColors.dart';

class StartScheduleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.icaWhite,
          centerTitle: true,
          title: StartScheduleTitleWidget(),
          leading: TextButton(
            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Icon(
                Icons.arrow_back_ios,
                color: AppColors.icaPurple,
                size: 24,
              ),
              Text(
                "Back",
                style: TextStyle(fontSize: 17, color: AppColors.icaPurple),
              ),
            ]),
            onPressed: () {},
          ),
          leadingWidth: 90,
        ),
        body: Center(
          child: Column(
            children: [
              SizedBox(height: 53),
              svgStartPlus,
              SizedBox(height: 53),
              LetsSetScheduleTextWidget(),
              SizedBox(height: 32),
              LetsSetAnymedTextWidget(),
              Spacer(),
              LetsBeginButtonWidget(),
            ],
          ),
        ));
  }
}

class StartScheduleTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text("Set a Schedule",
        style: TextStyle(fontSize: 17, color: AppColors.icaDarkGray));
  }
}

final String assetName = 'lib/images/start_plus.svg';
final Widget svgStartPlus = SvgPicture.asset(
  assetName,
  semanticsLabel: 'Acme Logo',
);

class LetsSetScheduleTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24),
      child: Text("Let's set a schedule that works for you",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontFamily: "Rubik",
              fontWeight: FontWeight.w600,
              fontSize: 24,
              color: AppColors.icaDarkGray)),
    );
  }
}

class LetsSetAnymedTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(left: 24, right: 24),
        child: Text(
            "Let's fit Anymed into your life. We'll walk through setting up a schedule that fits. You can always come back and change this.",
            textAlign: TextAlign.justify,
            style: TextStyle(
                fontFamily: "Rubik-Medium",
                fontWeight: FontWeight.w400,
                fontSize: 17,
                color: AppColors.icaDarkGray)));
  }
}

class LetsBeginButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, bottom: 40),
      child: ElevatedButton(
        onPressed: () {
          print("LetsBeginButton pressed");
        },
        style: ElevatedButton.styleFrom(
            primary: AppColors.icaPurple,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
        child: Padding(
          padding: EdgeInsets.only(
            top: 12,
            bottom: 12,
          ),
          child: Padding(
            padding: EdgeInsets.only(
              left: 24,
              right: 24,
            ),
            child: Row(children: [
              Spacer(),
              Text("Let's begin",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: "Rubik-Medium",
                      fontWeight: FontWeight.w600,
                      fontSize: 17,
                      color: AppColors.icaWhite)),
              Spacer(),
            ]),
          ),
        ),
      ),
    );
  }
}
