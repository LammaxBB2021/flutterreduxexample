import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:my_fisrt_flutter_app/design/AppColors.dart';

class NewToInjectingBiologicsWidget extends StatefulWidget {
  const NewToInjectingBiologicsWidget({Key? key}) : super(key: key);

  @override
  _NewToInjectingBiologicsWidget createState() =>
      _NewToInjectingBiologicsWidget();
}

class _NewToInjectingBiologicsWidget
    extends State<NewToInjectingBiologicsWidget> {
  int _selected = 0;

  void _handleSelectedChanged(int newValue) {
    setState(() {
      _selected = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: AppColors.icaWhite,
            centerTitle: true,
            title: YourAnymedTitleWidget(),
            leading: TextButton(
              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Icon(
                  Icons.arrow_back_ios,
                  color: AppColors.icaPurple,
                  size: 24,
                ),
                Text(
                  "Back",
                  style: TextStyle(fontSize: 17, color: AppColors.icaPurple),
                  maxLines: 1,
                  softWrap: false,
                ),
              ]),
              onPressed: () {},
            ),
            leadingWidth: 90,
            actions: <Widget>[
              Center(
                child: Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        print("Exit");
                      },
                      child: Text(
                        "Exit",
                        style: TextStyle(
                            fontFamily: "Rubik",
                            fontSize: 17,
                            color: AppColors.icaPurple),
                      ),
                    )),
              )
            ]),
        body: Center(
          child: Column(
            children: [
              Spacer(),
              AreYouNewToInjectingTextWidget(),
              Spacer(),
              PenWidget(),
              Spacer(),
              NewToInjectingButtonsWidget(
                  selected: _selected, onChanged: _handleSelectedChanged),
              Spacer(),
              ContinueButtonWidget(selected: _selected)
            ],
          ),
        ));
  }
}

class YourAnymedTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text("Set a Schedule",
        style: TextStyle(fontSize: 17, color: AppColors.icaDarkGray));
  }
}

class AreYouNewToInjectingTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24),
      child: Text(
          "Are you new to injecting biologics with a pre-filled syringe?",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontFamily: "Rubik",
              fontWeight: FontWeight.w600,
              fontSize: 24,
              color: AppColors.icaDarkGray)),
    );
  }
}

final String asset1Name = 'lib/images/sensoready_pen_small.svg';
final Widget sensoreadyPenSmall = SvgPicture.asset(
  asset1Name,
  semanticsLabel: 'Acme Logo',
);

class PenWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      width: 257,
      height: 231,
      child: sensoreadyPenSmall,
    ));
  }
}

class NewToInjectingButtonsWidget extends StatefulWidget {
  const NewToInjectingButtonsWidget({
    Key? key,
    required this.selected,
    required this.onChanged,
  }) : super(key: key);

  final int selected;
  final ValueChanged<int> onChanged;

  @override
  _NewToInjectingButtonsWidget createState() => _NewToInjectingButtonsWidget();
}

class _NewToInjectingButtonsWidget extends State<NewToInjectingButtonsWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24),
      child: Column(
        children: [
          TextButton(
            onPressed: () {
              widget.onChanged(widget.selected == 1 ? 0 : 1);
            },
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                    widget.selected == 1
                        ? AppColors.icaPurple
                        : AppColors.icaWhite),
                overlayColor:
                    MaterialStateProperty.all<Color>(AppColors.icaDarkGray15),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        side: BorderSide(
                            color: AppColors.icaDarkGray15,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(4)))),
            child: Padding(
              padding: EdgeInsets.only(
                top: 16,
                bottom: 13,
              ),
              child: Padding(
                padding: EdgeInsets.only(
                  left: 16,
                  right: 16,
                ),
                child: Row(children: [
                  Text("Yes - this is new to me",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: "Rubik",
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: widget.selected == 1
                              ? AppColors.icaWhite
                              : AppColors.icaDarkGray)),
                  Spacer(),
                ]),
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          TextButton(
            onPressed: () {
              widget.onChanged(widget.selected == 2 ? 0 : 2);
            },
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                    widget.selected == 2
                        ? AppColors.icaPurple
                        : AppColors.icaWhite),
                overlayColor:
                    MaterialStateProperty.all<Color>(AppColors.icaDarkGray15),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        side: BorderSide(
                            color: AppColors.icaDarkGray15,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(4)))),
            child: Padding(
              padding: EdgeInsets.only(
                top: 16,
                bottom: 10,
              ),
              child: Padding(
                padding: EdgeInsets.only(
                  left: 16,
                  right: 60,
                ),
                child: Row(children: [
                  Text("No - I’ve injected biologics in a\nsyringe before",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: "Rubik",
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: widget.selected == 2
                              ? AppColors.icaWhite
                              : AppColors.icaDarkGray)),
                  Spacer(),
                ]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ContinueButtonWidget extends StatelessWidget {
  const ContinueButtonWidget({required this.selected});

  final int selected;

  void _onContinuePressed() {
    print("_onContinuePressed");
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, bottom: 51),
      child: TextButton(
        onPressed: selected == 0 ? null : _onContinuePressed,
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                selected != 0 ? AppColors.icaPurple : AppColors.icaDarkGray15),
            overlayColor:
                MaterialStateProperty.all<Color>(AppColors.icaDarkGray15),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)))),
        child: Padding(
          padding: EdgeInsets.only(
            top: 12,
            bottom: 12,
          ),
          child: Padding(
            padding: EdgeInsets.only(
              left: 24,
              right: 24,
            ),
            child: Row(children: [
              Spacer(),
              Text("Continue",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: "Rubik-Medium",
                      fontWeight: FontWeight.w600,
                      fontSize: 17,
                      color: selected != 0
                          ? AppColors.icaWhite
                          : AppColors.icaBlack30)),
              Spacer(),
            ]),
          ),
        ),
      ),
    );
  }
}
