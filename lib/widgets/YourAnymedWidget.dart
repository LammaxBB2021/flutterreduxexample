import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:my_fisrt_flutter_app/design/AppColors.dart';

class YourAnymedWidget extends StatefulWidget {
  const YourAnymedWidget({Key? key}) : super(key: key);

  @override
  _YourAnymedWidget createState() => _YourAnymedWidget();
}

class _YourAnymedWidget extends State<YourAnymedWidget> {
  int _selected = 0;

  void _handleSelectedChanged(int newValue) {
    setState(() {
      _selected = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: AppColors.icaWhite,
            centerTitle: true,
            title: YourAnymedTitleWidget(),
            leading: TextButton(
              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Icon(
                  Icons.arrow_back_ios,
                  color: AppColors.icaPurple,
                  size: 24,
                ),
                Text(
                  "Back",
                  style: TextStyle(fontSize: 17, color: AppColors.icaPurple),
                  maxLines: 1,
                  softWrap: false,
                ),
              ]),
              onPressed: () {},
            ),
            leadingWidth: 90,
            actions: <Widget>[
              Center(
                child: Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        print("Exit");
                      },
                      child: Text(
                        "Exit",
                        style: TextStyle(
                            fontFamily: "Rubik",
                            fontSize: 17,
                            color: AppColors.icaPurple),
                      ),
                    )),
              )
            ]),
        body: Center(
          child: Column(
            children: [
              SizedBox(height: 32),
              WhatDoesYourAnymedTextWidget(),
              SizedBox(height: 35),
              PensWidget(
                  selected: _selected, onChanged: _handleSelectedChanged),
              Spacer(),
              PensButtonsWidget(selected: _selected)
            ],
          ),
        ));
  }
}

class YourAnymedTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text("Set a Schedule",
        style: TextStyle(fontSize: 17, color: AppColors.icaDarkGray));
  }
}

class WhatDoesYourAnymedTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24),
      child: Text("What does your Anymed come in?",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontFamily: "Rubik",
              fontWeight: FontWeight.w600,
              fontSize: 24,
              color: AppColors.icaDarkGray)),
    );
  }
}

final String asset1Name = 'lib/images/sensoready_pen_small.svg';
final Widget sensoreadyPenSmall = SvgPicture.asset(
  asset1Name,
  semanticsLabel: 'Acme Logo',
);

final String asset2Name = 'lib/images/unoready_pen_small.svg';
final Widget unoreadyPenSmall = SvgPicture.asset(
  asset2Name,
  semanticsLabel: 'Acme Logo2',
);

class PensWidget extends StatefulWidget {
  const PensWidget({
    Key? key,
    required this.selected,
    required this.onChanged,
  }) : super(key: key);

  final int selected;
  final ValueChanged<int> onChanged;

  @override
  _PensWidgetState createState() => _PensWidgetState();
}

class _PensWidgetState extends State<PensWidget> {
  ButtonStyle penButtonStyle = ButtonStyle(
      backgroundColor:
          MaterialStateProperty.all<Color>(AppColors.icaTransparent),
      overlayColor: MaterialStateProperty.all<Color>(AppColors.icaPurple10),
      shape: MaterialStateProperty.all<CircleBorder>(
          CircleBorder(side: BorderSide(color: Colors.transparent))));

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(children: [
            TextButton(
              style: penButtonStyle,
              onPressed: () {
                widget.onChanged(widget.selected == 1 ? 0 : 1);
              },
              child: CircleAvatar(
                  radius: 74,
                  backgroundColor: widget.selected == 1
                      ? AppColors.icaPurple
                      : AppColors.icaDarkGray15,
                  child: CircleAvatar(
                    radius: 73,
                    backgroundColor: AppColors.icaWhite,
                    child: CircleAvatar(
                      radius: 73,
                      backgroundColor: widget.selected == 1
                          ? AppColors.icaPurple10
                          : AppColors.icaWhite,
                      child: sensoreadyPenSmall,
                    ),
                  )),
            ),
            SizedBox(height: 8),
            Text(
              "Sensoready Pen",
              style: TextStyle(
                  fontFamily: "Rubik",
                  fontSize: 17,
                  color: AppColors.icaDarkGray),
            ),
          ]),
          SizedBox(width: 27),
          Column(children: [
            TextButton(
              style: penButtonStyle,
              onPressed: () {
                widget.onChanged(widget.selected == 2 ? 0 : 2);
              },
              child: CircleAvatar(
                  radius: 74,
                  backgroundColor: widget.selected == 2
                      ? AppColors.icaPurple
                      : AppColors.icaDarkGray15,
                  child: CircleAvatar(
                    radius: 73,
                    backgroundColor: AppColors.icaWhite,
                    child: CircleAvatar(
                      radius: 73,
                      backgroundColor: widget.selected == 2
                          ? AppColors.icaPurple10
                          : AppColors.icaWhite,
                      child: unoreadyPenSmall,
                    ),
                  )),
            ),
            SizedBox(height: 8),
            Text(
              "Unoready Pen",
              style: TextStyle(
                  fontFamily: "Rubik",
                  fontSize: 17,
                  color: AppColors.icaDarkGray),
            ),
          ]),
        ],
      ),
    );
  }
}

class PensButtonsWidget extends StatelessWidget {
  const PensButtonsWidget({required this.selected});

  final int selected;

  void _onContinuePressed() {
    print("_onContinuePressed");
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, bottom: 51),
      child: Column(
        children: [
          TextButton(
            onPressed: selected == 0 ? null : _onContinuePressed,
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(selected != 0
                    ? AppColors.icaPurple
                    : AppColors.icaDarkGray15),
                overlayColor:
                    MaterialStateProperty.all<Color>(AppColors.icaDarkGray15),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)))),
            child: Padding(
              padding: EdgeInsets.only(
                top: 12,
                bottom: 12,
              ),
              child: Padding(
                padding: EdgeInsets.only(
                  left: 24,
                  right: 24,
                ),
                child: Row(children: [
                  Spacer(),
                  Text("Continue",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: "Rubik-Medium",
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: selected != 0
                              ? AppColors.icaWhite
                              : AppColors.icaBlack30)),
                  Spacer(),
                ]),
              ),
            ),
          ),
          SizedBox(
            height: 13,
          ),
          TextButton(
            onPressed: () {
              print("I don't know pressed");
            },
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color>(AppColors.icaWhite),
                overlayColor:
                    MaterialStateProperty.all<Color>(AppColors.icaDarkGray15),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)))),
            child: Padding(
              padding: EdgeInsets.only(
                top: 12,
                bottom: 12,
              ),
              child: Padding(
                padding: EdgeInsets.only(
                  left: 24,
                  right: 24,
                ),
                child: Row(children: [
                  Spacer(),
                  Text("I don't know",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: "Rubik-Medium",
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: AppColors.icaPurple)),
                  Spacer(),
                ]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
