import 'package:flutter/material.dart';

class AppColors {
  static const Color icaTransparent = Colors.transparent;
  static const Color icaWhite = Colors.white;
  static const Color icaPurple = Color(0XFF981E97); //orange Color(0XFFF48B4A);
  static const Color icaPurple10 =
      Color(0X10981E97); //orange10Color(0X10F48B4A);
  static const Color icaDarkGray =
      Color(0XFF2D2927); //darkBlue Color(0xFF354263);
  static const Color icaDarkGray15 =
      Color(0X262D2927); //DarkBlue15 Color(0x26354263);
  static const Color icaBlack30 = Color(0x1E000000);
}
