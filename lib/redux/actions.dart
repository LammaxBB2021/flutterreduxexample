import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'app_state.dart';

class AddAction {}

class RemoveAction {}

class ResetTextAction {}

class ResetWidgetAction {}

class SetTextAction {
  final String text;

  SetTextAction({required this.text});
}

class GetImageAction {
  final String imageURL;

  GetImageAction({required this.imageURL});

  late ThunkAction<AppState> getImage = (Store<AppState> store) {
    store.dispatch(GetImageAction(imageURL: imageURL));

    _loadImage(imageURL)
        .then((image) => store.dispatch(LoadedImageAction(image)));
  };

  Future<Widget> _loadImage(String url) async {
    await Future.delayed(Duration(seconds: 2));
    return Image.network(url);
  }
}

class LoadedImageAction {
  final Widget widget;

  LoadedImageAction(this.widget);
}
