import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'actions.dart';
import 'app_state.dart';

AppState reducer(AppState state, dynamic action) => AppState(
      counter: _counterReducer(state.counter, action),
      text: _textReducer(state.text, action),
      widget: _imageReducer(state.widget, action),
    );

Widget? _getImageReducer(Widget? widget, GetImageAction action) =>
    Center(child: CircularProgressIndicator());
Widget? _loadedImageReducer(Widget? widget, LoadedImageAction action) =>
    action.widget;
Widget? _resetWidgetReducer(Widget? widget, ResetWidgetAction action) => null;

String _setTextReducer(String text, SetTextAction action) => action.text;
String _resetTextReducer(String text, ResetTextAction action) => "";

int _addCounterReducer(int count, AddAction action) => count + 1;
int _removeCounterReducer(int count, RemoveAction action) => count - 1;

Reducer<Widget?> _imageReducer = combineReducers([
  TypedReducer<Widget?, GetImageAction>(_getImageReducer),
  TypedReducer<Widget?, LoadedImageAction>(_loadedImageReducer),
  TypedReducer<Widget?, ResetWidgetAction>(_resetWidgetReducer),
]);

Reducer<int> _counterReducer = combineReducers([
  TypedReducer<int, AddAction>(_addCounterReducer),
  TypedReducer<int, RemoveAction>(_removeCounterReducer),
]);

Reducer<String> _textReducer = combineReducers([
  TypedReducer<String, SetTextAction>(_setTextReducer),
  TypedReducer<String, ResetTextAction>(_resetTextReducer),
]);

/*
AppState reducer(AppState state, dynamic action) => AppState(
    counter: _counterReducer(state, action), text: _textReducer(state, action));

int _counterReducer(AppState state, dynamic action) {
  switch (action.runtimeType) {
    case AddAction:
      return state.counter + 1;
    case RemoveAction:
      return state.counter - 1;
    default:
      return state.counter;
  }
}

String _textReducer(AppState state, dynamic action) {
  switch (action.runtimeType) {
    case ResetTextAction:
      return "";
    case SetTextAction:
      return action.text;
    default:
      return state.text;
  }
}
*/
