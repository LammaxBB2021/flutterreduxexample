import 'package:flutter/cupertino.dart';
import 'package:my_fisrt_flutter_app/redux/actions.dart';
import 'package:redux/redux.dart';

import 'app_state.dart';

void loaderMiddleware(
    Store<AppState> store, dynamic action, NextDispatcher nextDispatcher) {
  if (action is GetImageAction) {
    _loadImage(action.imageURL)
        .then((image) => store.dispatch(LoadedImageAction(image)));
  }

  nextDispatcher(action);
}

Future<Widget> _loadImage(String url) async {
  await Future.delayed(Duration(seconds: 2));
  return Image.network(url);
}
